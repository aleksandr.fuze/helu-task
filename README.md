## Requirements
Python >= 3.10  
[Poetry](https://python-poetry.org/docs/) >= 1.1.14

## Local configuration
### Installing poetry
#### osx / linux
`curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -`  
#### Windows
`(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -`

### Install env and dependencies
`poetry install`
### Activating virtual env
`poetry shell`

## Run application
`python run.py`

## Tests
`pytest -v .`

## Docker 
### Local run
`docker-compose up`

## Other
### swagger/Open API
`{host}/docs`  
`{host}/redoc`  

### Updating dependencies to their latest versions
poetry update
