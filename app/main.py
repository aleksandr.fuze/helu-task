# -*- coding: utf-8 -*-
"""Creating and configure an application"""

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from loguru import logger

from app.routers import router


def create_app() -> FastAPI:
    """
    Function that creates web application
    Returns:
        object: FastAPI instance
    """
    logger.info("creating application")
    app_instance = FastAPI()
    # add routes table
    app_instance.include_router(router)
    # enable CORS
    app_instance.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    return app_instance
