# -*- coding: utf-8 -*-
import pandas as pd

from app.storage.data_loader import load_data


def _insert_month_analytics(df: pd.DataFrame, key_fields: list | tuple) -> pd.DataFrame:
    """Calculates and inserts comparison analytics"""
    if not key_fields or len(key_fields) != 2:
        return df

    first, second = key_fields
    june_vs_may_rev = df[first]["Revenues"] - df[second]["Revenues"]
    june_vs_may_exp = df[first]["Expenses"] - df[second]["Expenses"]
    june_vs_may_prof = df[first]["Profits"] - df[second]["Profits"]
    june_vs_may_marg = float(df[first]["Margins"][:-1]) - float(df[second]["Margins"][:-1])

    df["June vs May, 2020 (Abs)"] = [
        round(june_vs_may_rev, 2),
        round(june_vs_may_exp, 2),
        june_vs_may_prof,
        f"{june_vs_may_marg:.1f} p.p.",
    ]
    df["June vs May, 2020 (%)"] = [
        f"{june_vs_may_rev/abs(df[second]['Revenues']):.1%}",
        f"{june_vs_may_exp/abs(df[second]['Expenses']):.1%}",
        f"{june_vs_may_prof/abs(df[second]['Profits']):.1%}",
        "-"
    ]
    return df


def calculate_analytics() -> pd.DataFrame:
    """Calculates analytics, prepares a report"""
    data_frame = load_data()
    res = pd.DataFrame(index=pd.Index(['Revenues', 'Expenses', 'Profits', 'Margins'], dtype='object'))
    data_frame = data_frame.query("month in ('May','June') and year == 2020")
    for month, row in data_frame.groupby(["month"]):
        income_credit = row.query("transaction_type == 'credit' and account_nature == 'income'")["amount"]
        income_debit = row.query("transaction_type == 'debit' and account_nature == 'income'")["amount"]
        expense_credit = row.query("transaction_type == 'credit' and account_nature == 'expense'")["amount"]
        expense_debit = row.query("transaction_type == 'debit' and account_nature == 'expense'")["amount"]
        revenues = float(income_credit) - float(income_debit)
        expenses = float(expense_credit) - float(expense_debit)
        profits = revenues + expenses
        res[f"{month}, 2020"] = [round(revenues, 2), round(expenses, 2), round(profits, 2), f"{profits/revenues:.1%}"]

    _insert_month_analytics(res, ["June, 2020", "May, 2020"])
    return res
