# -*- coding: utf-8 -*-
"""Routes table for an image compression"""

from io import StringIO

from loguru import logger
from fastapi import APIRouter

from app.services.analytics import calculate_analytics
from fastapi.responses import StreamingResponse

router = APIRouter()


@router.get("/ping")
async def ping():
    """Healthcheck"""
    logger.info("healthcheck request detected")
    return {"ping": "pong!"}


@router.get("/report")
async def generate_report():
    """Analytics report"""
    logger.info("report request detected")
    analytics_rep = calculate_analytics()
    response = StreamingResponse(StringIO(analytics_rep.to_csv(mode="w", line_terminator="\n")), media_type="text/csv")
    return response
