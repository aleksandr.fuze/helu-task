# -*- coding: utf-8 -*-
import os

import pandas as pd


def load_data() -> pd.DataFrame:
    """Loads source data, prepares a data frame"""
    bookings_path = os.path.join('data', 'bookings.csv')
    chat_of_accounts_path = os.path.join('data', 'chart-of-accounts.csv')
    type_dict = {'account_code': 'int64', 'transaction_type': 'object', 'account_nature': 'object'}
    bookings_df = pd.read_csv(bookings_path, dtype=type_dict, parse_dates=["transaction_date"])
    chat_of_accounts_df = pd.read_csv(chat_of_accounts_path)
    res_df = pd.merge(bookings_df, chat_of_accounts_df, on="account_code", how="inner")
    res_df["amount"] = res_df["amount"].replace(r',', r'.', regex=True).astype(float)
    res_df = res_df.groupby(
        [res_df['transaction_date'].dt.year.rename('year'),
         res_df['transaction_date'].dt.month_name().rename('month'),
         res_df["transaction_type"],
         res_df["account_nature"]]
    )['amount'].sum().reset_index()
    return res_df
