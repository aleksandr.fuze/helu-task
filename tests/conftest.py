# -*- coding: utf-8 -*-
import pytest

from app.main import create_app


@pytest.fixture(scope="module")
def test_app():
    yield create_app()
