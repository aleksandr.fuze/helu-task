# -*- coding: utf-8 -*-
import pytest
from httpx import AsyncClient


@pytest.mark.asyncio
async def test_ping(test_app):
    async with AsyncClient(app=test_app, base_url="http://test") as cli:
        response = await cli.get("/ping")

    assert response.status_code == 200
    assert response.json() == {"ping": "pong!"}


@pytest.mark.asyncio
async def test_report(test_app, file_regression):
    """
    Given a GET request to an endpoint /report,
    The response should match with provided test_report.csv file
    """
    async with AsyncClient(app=test_app, base_url="http://test") as ac:
        response = await ac.get("/report")

    file_regression.check(response.content, extension=".csv", binary=True)
