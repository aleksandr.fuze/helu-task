# -*- coding: utf-8 -*-
"""Script for running an application"""

import argparse

import uvicorn


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', default='0.0.0.0')
    parser.add_argument('--port', default=8000, type=int)
    args = parser.parse_args()
    uvicorn.run("app.main:create_app", host=args.host, port=args.port, reload=True, factory=True)
